﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SDFGenerator))]
public class SDFGeneratorEditor : Editor {

	private double startScanTime;
	private bool scanning;

	private SDFGenerator myTarget;

	public override void OnInspectorGUI ()
	{
		myTarget = (SDFGenerator)target;
		DrawDefaultInspector ();
		if (GUILayout.Button ("Load model")) {
			Debug.Log ("Loading model data...");
			double startFunction = EditorApplication.timeSinceStartup;
			myTarget.StartScanDistanceField ();
			Debug.Log ("Data loaded in "+(EditorApplication.timeSinceStartup-startFunction)+" seconds");
		}
		if (GUILayout.Button ("Scan")) {
			startScanTime = EditorApplication.timeSinceStartup;
			scanning = true;
			EditorApplication.update += TrackScanProcess;
			Debug.Log ("Scanning model data...");
			myTarget.BeginParalleScan ();
		}

		if (GUILayout.Button ("Generate Distance Field")) {
			if (myTarget.DataCollectingDone ()) {
				string path = AssetDatabase.GenerateUniqueAssetPath (myTarget.outputDataPath);
				SDFData asset = ScriptableObject.CreateInstance<SDFData> ();
				asset.Initialize (myTarget.myColorArray, myTarget.mapSize, "Signed Distance Field");

				AssetDatabase.CreateAsset (asset, path);
				AssetDatabase.AddObjectToAsset (asset.texture, asset);
			} else
				Debug.LogWarning ("Data collection not done, please wait");
		}
		if (GUILayout.Button ("Abort Threads")) {
			myTarget.AbortAllRunningThread ();
		}

		if (GUILayout.Button ("ReconstructMesh")) {
			myTarget.StartScanDistanceField ();
			myTarget.ReconstructMesh ();
		}

		if (GUILayout.Button ("Scan one layer")) {
			myTarget.ScanLayerAt ();
		}

		if (GUILayout.Button ("Print data")) {
			myTarget.PrintDataManual ();
		}
	}

	void TrackScanProcess()
	{
		if (scanning && myTarget.DataCollectingDone ()) {
			EditorApplication.update += TrackScanProcess;
			scanning = false;
			Debug.Log ("Scanning done in " + (EditorApplication.timeSinceStartup - startScanTime) + " seconds.");
			myTarget.AbortAllRunningThread ();
		}
	}
}
