﻿using UnityEngine;

public class SDFData : ScriptableObject {

	[SerializeField] Texture3D _texture;

	public Texture3D texture {
		get { return _texture; }
	}

	#if UNITY_EDITOR

	#region Editor functions

	public void Initialize(Color32[] dataInput, int size, string name)
	{
		_texture = new Texture3D(size, size, size, TextureFormat.RGB24, true);

		_texture.name = name;
		_texture.filterMode = FilterMode.Bilinear;
		_texture.wrapMode = TextureWrapMode.Clamp;
		_texture.SetPixels32(dataInput);
		_texture.Apply();
	}

	#endregion

	#endif
}