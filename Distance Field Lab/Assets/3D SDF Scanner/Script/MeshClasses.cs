﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triangle
{
	private Vector3 p1, p2, p3;
	private Vector3 v1, v2, v3;
	private Vector3 normal;
	private float[] transformation;
	public int[] e;

	public Triangle(Vector3 a, Vector3 b, Vector3 c)
	{
		p1 = a;
		p2 = b;
		p3 = c;
		e = new int[3];
		v1 = (p1 - p2).normalized + (p1 - p3).normalized;
		v2 = (p2 - p1).normalized + (p2 - p3).normalized;
		v3 = (p3 - p1).normalized + (p3 - p2).normalized;
		normal = Vector3.Cross((p2-p1).normalized,(p3-p1).normalized).normalized;
		if (normal == Vector3.zero) {
			if (p2 == p3) {
				return;
			} else {
				normal = Vector3.Cross ((p1 - p3).normalized, (p2 - p3).normalized).normalized;
			}
		}
		if (normal == Vector3.zero) {
			if (p1 == p2) {
				return;
			} else {
				normal = Vector3.Cross ((p3 - p2).normalized, (p1 - p2).normalized).normalized;
			}
		}

		transformation = new float[4];
		if (Mathf.Abs (normal.x) >= Mathf.Abs (normal.y) && Mathf.Abs (normal.x) >= Mathf.Abs (normal.z)) {
			transformation [0] = 1.0f;
			transformation [1] = normal.y / normal.x;
			transformation [2] = normal.z / normal.x;
			transformation [3] = -Vector3.Dot (p1, normal) / normal.x;
		} else if (Mathf.Abs (normal.y) >= Mathf.Abs (normal.z)) {
			transformation [0] = normal.x / normal.y;
			transformation [1] = 1.0f;
			transformation [2] = normal.z / normal.y;
			transformation [3] = -Vector3.Dot (p1, normal) / normal [1];
		} else if (Mathf.Abs (normal.z) > 0) {
			transformation[0] = normal.x / normal.z;
			transformation[1] = normal.y / normal.z;
			transformation[2] = 1.0f;
			transformation[3] = -Vector3.Dot (p1, normal) / normal[2];
		} else
			Debug.Log ("Hey this triangle is fucked up, someone fix this");
	}

	public bool ContainVerticle(Vector3 inputVector)
	{
		return inputVector == p1 || inputVector == p2 || inputVector == p3;
	}

	public void UpdateData(Vector3 a, Vector3 b, Vector3 c)
	{
		p1 = a;
		p2 = b;
		p3 = c;
		v1 = (p1 - p2).normalized + (p1 - p3).normalized;
		v2 = (p2 - p1).normalized + (p2 - p3).normalized;
		v3 = (p3 - p1).normalized + (p3 - p2).normalized;
		normal = Vector3.Cross((p2-p1).normalized,(p3-p1).normalized).normalized;
		if (normal == Vector3.zero) {
			if (p2 == p3) {
				return;
			} else {
				normal = Vector3.Cross ((p1 - p3).normalized, (p2 - p3).normalized).normalized;
			}
		}
		if (normal == Vector3.zero) {
			if (p1 == p2) {
				return;
			} else {
				normal = Vector3.Cross ((p3 - p2).normalized, (p1 - p2).normalized).normalized;
			}
		}

		transformation = new float[4];
		if (Mathf.Abs (normal.x) >= Mathf.Abs (normal.y) && Mathf.Abs (normal.x) >= Mathf.Abs (normal.z)) {
			transformation [0] = 1.0f;
			transformation [1] = normal.y / normal.x;
			transformation [2] = normal.z / normal.x;
			transformation [3] = -Vector3.Dot (p1, normal) / normal.x;
		} else if (Mathf.Abs (normal.y) >= Mathf.Abs (normal.z)) {
			transformation [0] = normal.x / normal.y;
			transformation [1] = 1.0f;
			transformation [2] = normal.z / normal.y;
			transformation [3] = -Vector3.Dot (p1, normal) / normal [1];
		} else if (Mathf.Abs (normal.z) > 0) {
			transformation[0] = normal.x / normal.z;
			transformation[1] = normal.y / normal.z;
			transformation[2] = 1.0f;
			transformation[3] = -Vector3.Dot (p1, normal) / normal[2];
		} else
			Debug.Log ("Hey this triangle is fucked up, someone fix this, please");
	}

	public bool IntersectWithRay(Vector3 origin, Vector3 targetPoint, out Vector3 dataOut)
	{
		Vector3 intersection = targetPoint;

		Vector3 direction = targetPoint - origin;
		targetPoint += direction * 0.001f;
		float mag = direction.magnitude;

		float transS = transformation[0] * origin.x + transformation[1] * origin.y + transformation[2] * origin.z + transformation[3];
		float transD = transformation[0] * direction.x + transformation[1] * direction.y + transformation[2] * direction.z;

		float ta = -transS / transD;

		dataOut = targetPoint;

		// Reject negative t values and rays parallel to triangle
		if ( ta <= 0.0000001f || ta >= 10000)
			return false;


		// Get global coordinates of ray's intersection with triangle's plane.
		intersection.x = origin.x + ta * direction.x;
		intersection.y = origin.y + ta * direction.y;
		intersection.z = origin.z + ta * direction.z;

		if (Vector3.Distance (origin, intersection) > Vector3.Distance (origin, targetPoint))
			return false;

		dataOut = intersection;

		float f1, f2, f3;
		f1 = Vector3.Dot (Vector3.Cross (v1, (intersection - p1)), normal);
		f2 = Vector3.Dot (Vector3.Cross (v2, (intersection - p2)), normal);
		f3 = Vector3.Dot (Vector3.Cross (v3, (intersection - p3)), normal);
		bool outside = false;
		if (f1 > 0 && f2 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((intersection - p1), (intersection - p2)), normal) < 0) // outside
				outside = true;
		} else if (f2 > 0 && f3 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((intersection - p2), (intersection - p3)), normal) < 0) // outside
				outside = true;
		} else if (f3 > 0 && f1 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((intersection - p3), (intersection - p1)), normal) < 0)
				outside = true;
		}

		if (!outside) {
			return true;
		}

		return false;
	}

	public bool IntersectWithRay(Vector3 origin, Vector3 targetPoint)
	{
		Vector3 intersection = targetPoint;

		Vector3 direction = targetPoint - origin;
		targetPoint += direction * 0.001f;
		float mag = direction.magnitude;

		float transS = transformation[0] * origin.x + transformation[1] * origin.y + transformation[2] * origin.z + transformation[3];
		float transD = transformation[0] * direction.x + transformation[1] * direction.y + transformation[2] * direction.z;

		float ta = -transS / transD;


		// Reject negative t values and rays parallel to triangle
		if ( ta <= 0.0000001f || ta >= 10000)
			return false;


		// Get global coordinates of ray's intersection with triangle's plane.
		intersection.x = origin.x + ta * direction.x;
		intersection.y = origin.y + ta * direction.y;
		intersection.z = origin.z + ta * direction.z;

		if (Vector3.Distance (origin, intersection) > Vector3.Distance (origin, targetPoint))
			return false;

		float f1, f2, f3;
		f1 = Vector3.Dot (Vector3.Cross (v1, (intersection - p1)), normal);
		f2 = Vector3.Dot (Vector3.Cross (v2, (intersection - p2)), normal);
		f3 = Vector3.Dot (Vector3.Cross (v3, (intersection - p3)), normal);
		bool outside = false;
		if (f1 > 0 && f2 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((intersection - p1), (intersection - p2)), normal) < 0) // outside
				outside = true;
		} else if (f2 > 0 && f3 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((intersection - p2), (intersection - p3)), normal) < 0) // outside
				outside = true;
		} else if (f3 > 0 && f1 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((intersection - p3), (intersection - p1)), normal) < 0)
				outside = true;
		}

		if (!outside) {
			return true;
		}

		return false;
	}

	public Vector3 NormalVector()
	{
		return normal;
	}

	public Vector3 ProjectOnToPlane(Vector3 inputPoint)
	{
		float length = Vector3.Dot((p1-inputPoint), normal);
		return inputPoint+length * normal;
	}

	public Vector3 ClosestPoint(Vector3 inputPoint, float length = Mathf.Infinity)
	{
		if (p1 == p2 && p1 == p3) {
			return p1;
		}

		if (p1 == p2||p3 == p2) {
			if (Vector3.Distance (p1, inputPoint) < Vector3.Distance (p3, inputPoint))
				return p1;
			else
				return p3;
		}

		if (p1 == p3) {
			if (Vector3.Distance (p1, inputPoint) < Vector3.Distance (p2, inputPoint))
				return p1;
			else
				return p2;
		}



		Vector3 p0, p_1, p_2, p_3;
		float f1, f2, f3;
		p0 = ProjectOnToPlane (inputPoint);
		f1 = Vector3.Dot (Vector3.Cross (v1, (p0 - p1)), normal);
		f2 = Vector3.Dot (Vector3.Cross (v2, (p0 - p2)), normal);
		f3 = Vector3.Dot (Vector3.Cross (v3, (p0 - p3)), normal);
		bool outside = false;
		if (f1 > 0 && f2 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((p0 - p1), (p0 - p2)), normal) < 0) // outside
				outside = true;
		} else if (f2 > 0 && f3 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((p0 - p2), (p0 - p3)), normal) < 0) // outside
				outside = true;
		} else if (f3 > 0 && f1 <= 0) {
			if (Vector3.Dot (Vector3.Cross ((p0 - p3), (p0 - p1)), normal) < 0)
				outside = true;
		}
		if (!outside) {
			return p0;
		}
		//else
		p_1 = Vector3.Project (p0-p2, p1 - p2);
		p_1 = p_1 + p2;
		if (Vector3.Dot((p_1-p1),(p_1-p2))>0) // same direction, which means p_1 is outside the edge
		{
			if ((p_1 - p1).magnitude < (p_1 - p2).magnitude) // p1 is closer to p_1 than p2
				p_1 = p1;
			else
				p_1 = p2;
		}

		p_2 = Vector3.Project (p0-p3, p2 - p3);
		p_2 = p_2 + p3;
		if (Vector3.Dot((p_2-p2),(p_2-p3))>0) // same direction, which means p_2 is outside the edge
		{
			if ((p_2 - p2).magnitude < (p_2 - p3).magnitude) // p2 is closer to p_2 than p3
				p_2 = p2;
			else
				p_2 = p3;
		}

		p_3 = Vector3.Project (p0-p1, p3 - p1);
		p_3 = p_3 + p1;
		if (Vector3.Dot((p_3-p3),(p_3-p1))>0) // same direction, which means p_3 is outside the edge
		{
			if ((p_3 - p3).magnitude < (p_3 - p1).magnitude) // p3 is closer to p_3 than p1
				p_3 = p3;
			else
				p_3 = p1;
		}
		if (Vector3.Distance (p0, p_1) > Vector3.Distance (p0, p_2))
			p_1 = p_2;
		if (Vector3.Distance (p0, p_1) > Vector3.Distance (p0, p_3))
			p_1 = p_3;
		return p_1;
	}

	public bool Irrelevante(float inputZ, float radius)
	{
		if (p1.z > inputZ + radius && p2.z > inputZ + radius && p3.z > inputZ + radius
			|| p1.z < inputZ - radius && p2.z < inputZ - radius && p3.z < inputZ - radius)
					return true;
		return false;
	}
}

public class Edge
{
	public int p1, p2;

	public Edge(int point1, int point2)
	{
		p1 = point1;
		p2 = point2;
	}

	public bool Contain(int index1, int index2)
	{
		return (index1 == p1 && index2 == p2) || (index1 == p2 && index2 == p1);
	}
}