﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;


// this script use multi-thread to speed up the process. If you have less than 4 core, please change the code
public class SDFGenerator : MonoBehaviour {

	public string outputDataPath = "Assets/3D SDF Scanner/Test Case Data/New SDF Data.asset";

	[Range(1,128)]
	public int mapSize;

	[Range(0.0f,1.0f)]
	public float blendRadius;

	[Range(-0.5f,0.5f)]
	public float angleCosinThreshold = 0.2f;

	public int layerToScan = 32;

	public GameObject targetMeshObject;

	Vector3 startPosition = Vector3.zero;
	float voxelSize;
	float blendRadiusDivider;

	[HideInInspector]
	public Color32[] myColorArray;

	Mesh targetMesh;
	Vector3 meshPosition, meshScale;
	Quaternion meshRotation;
	Vector3[] a_vertex;
	int[] a_triangle;
	Triangle[] a_trisPlane;
	Edge[] a_edge;
	int totalTris, totalEdge, totalVerticle;
	private List<int>[] trisBelongToVerticle;
	private int[][] trisBelongToEdge;

	public int trisIndex, edgeIndex, verticleIndex;

	private Vector3 a, b, c;

	public int coreNumber = 4;
	private bool threadAbort = false;
	private Thread[] a_thread;
	private float timeStart;
	private bool[] dataScanDone;

	public MeshFilter reconstructMeshVisualize;

	public bool drawCustomGizmos = true;
	Vector3 verticleNormal;

	public int layer;

	// animation
	public bool runAnimation;
	bool oldBool;
	float startTheAnimation, gMinDistance, gDistance;
	public int gX, gY, gT;
	public GameObject visualVoxel;
	private Vector3 targetDistancePoint, closestPoint, intersectPoint;
	private bool onVerticle, onEdge;
	private int edgeCutIndex, verticleCutIndex;

	void OnValidate()
	{
		mapSize = Mathf.ClosestPowerOfTwo (mapSize);
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireCube(this.transform.position + Vector3.up * 0.5f, Vector3.one);
		if (!drawCustomGizmos)
			return;
		trisIndex = Mathf.Clamp (trisIndex, 0, Mathf.Max(totalTris-1,0));
		if (a_vertex == null || a_vertex.Length == 0)
			return;
		a = a_vertex [a_triangle [trisIndex * 3]];
		b = a_vertex [a_triangle [trisIndex * 3 + 1]];
		c = a_vertex [a_triangle [trisIndex * 3 + 2]];
		Gizmos.DrawLine (a, b);
		Gizmos.DrawLine (b, c);
		Gizmos.DrawLine (c, a);
		if (a_trisPlane == null || a_trisPlane.Length == 0)
			return;
		Gizmos.DrawLine ((a+b+c)*0.3333333f, (a+b+c)*0.3333333f+a_trisPlane [trisIndex].NormalVector ()*0.15f);

		Gizmos.color = Color.yellow;

		verticleIndex = Mathf.Clamp (verticleIndex, 0, Mathf.Max (0, totalVerticle - 1));

		Gizmos.DrawSphere (a_vertex [verticleIndex], 0.002f);
		for (int i = 0; i < trisBelongToVerticle [verticleIndex].Count; i++) {
			DrawTrisOnGizmos (trisBelongToVerticle [verticleIndex] [i]);
		}
		verticleNormal.x = verticleNormal.y = verticleNormal.z;
		for (int i = 0; i< trisBelongToVerticle[verticleIndex].Count;i++)
		{
			verticleNormal += a_trisPlane [trisBelongToVerticle [verticleIndex] [i]].NormalVector ();
		}
		verticleNormal = verticleNormal.normalized;
		Gizmos.DrawLine (a_vertex [verticleIndex], a_vertex [verticleIndex] + verticleNormal);

		if (a_edge == null || a_edge.Length == 0)
			return;
		edgeIndex = Mathf.Clamp (edgeIndex, 0, Mathf.Max(0,totalEdge-1));
		Gizmos.color = Color.white;
		Gizmos.DrawSphere (a_vertex [a_edge [edgeIndex].p1], 0.002f);
		Gizmos.DrawSphere (a_vertex [a_edge [edgeIndex].p2], 0.002f);
		DrawTrisOnGizmos (trisBelongToEdge[edgeIndex][0]);
		DrawTrisOnGizmos (trisBelongToEdge[edgeIndex][1]);
		if (trisBelongToEdge [edgeIndex] [1] == -1)
			return;
		
		/*for (int i = 0; i < totalEdge; i++) {
			Gizmos.DrawLine (a_vertex [a_edge [i].p1], a_vertex [a_edge [i].p2]);
		}*/
	}

	void DrawTrisOnGizmos(int index)
	{
		Gizmos.DrawLine (a_vertex [a_triangle [index*3]], a_vertex [a_triangle [index* 3 + 1] ]);
		Gizmos.DrawLine (a_vertex [a_triangle [index*3+1]], a_vertex [a_triangle [index * 3 + 2]]);
		Gizmos.DrawLine (a_vertex [a_triangle [index*3+2]], a_vertex [a_triangle [index * 3]]);
	}

	void Update()
	{
		if (runAnimation && !oldBool) {
			startTheAnimation = Time.time;
			if (a_vertex == null || a_vertex.Length == 0)
				StartScanDistanceField ();
			visualVoxel.transform.localScale = Vector3.one * voxelSize;
			gMinDistance = 1;
		}
		if (runAnimation) {
			if (Time.time - startTheAnimation > 0) {
				ScanLayerAnimation ();
				if (gY == mapSize) {
					runAnimation = false;
					Debug.Log ("Finish scan a layer in "+ (Time.time-startTheAnimation));
				}
			}
		}
		oldBool = runAnimation;
	}

	void OnDestroy()
	{
		AbortAllRunningThread ();
	}

	void OnApplicationQuit()
	{
		AbortAllRunningThread ();
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	public void PrintDataManual()
	{
		GameObject gO;
		for (int x = 0; x < mapSize; x++) {
			for (int y = 0; y < mapSize; y++) {
				gO = Instantiate (visualVoxel);
				gO.transform.position = new Vector3((-0.5f*mapSize+0.5f+x)*voxelSize, (0.5f+y)*voxelSize, (0.5f*mapSize-0.5f-layer)*voxelSize);
				gO.GetComponent<SpriteRenderer> ().color = myColorArray[x+y*mapSize+layer*mapSize*mapSize];
			}
		}
	}

	public void AbortAllRunningThread()
	{
		if (a_thread == null) {
			return;
		}
		for (int i = 0; i < a_thread.Length; i++) {
			int lIndex = i;
			a_thread [lIndex].Abort ();
		}

		threadAbort = true;
	}

	public bool DataCollectingDone()
	{
		for (int i = 0; i < coreNumber; i++) {
			if (!dataScanDone [i])
				return dataScanDone [i];
		}
		return true;
	}

	public void StartScanDistanceField()
	{
		int i, j, counter, length; // two temp variable

		targetMesh = targetMeshObject.GetComponent<MeshFilter> ().sharedMesh; // get target mesh

		visualVoxel.transform.localScale = Vector3.one * voxelSize;

		// get transform information

		meshPosition = targetMeshObject.transform.position;
		meshRotation = targetMeshObject.transform.rotation;
		meshScale = targetMeshObject.transform.localScale;

		// get verticles and triangles raw data
		a_triangle = targetMesh.triangles;
		List<Vector3> verticlesList = new List<Vector3>();
		targetMesh.GetVertices (verticlesList);

		// merge duplicate vertex and re-assign unique verticles to triangle
		totalVerticle = verticlesList.Count;
		totalTris = a_triangle.Length / 3;
		for (i = 0; i < totalVerticle; i++) {
			for (j = i+1; j < totalVerticle; j++) {
				if (verticlesList [j] == verticlesList [i]) {
					for (int k = 0; k < totalTris; k++) {
						if (a_triangle [k * 3] == j)
							a_triangle [k * 3] = i;
						if (a_triangle [k * 3+1] == j)
							a_triangle [k * 3+1] = i;
						if (a_triangle [k * 3+2] == j)
							a_triangle [k * 3+2] = i;
					}
					verticlesList [j] = Vector3.one*Mathf.Infinity;
				}
			}
		}

		// remove all invalid verticle and re-assign verticle index to triangle
		for (i = 0; i < verticlesList.Count;) {
			if (verticlesList [i].x == Mathf.Infinity) {
				verticlesList.RemoveAt (i);
				for (j = 0; j < totalTris; j++) {
					if (a_triangle [j * 3] >= i)
						a_triangle [j * 3]--;
					if (a_triangle [j * 3+1]>=i)
						a_triangle [j * 3+1]--;
					if (a_triangle [j * 3+2]>=i)
						a_triangle [j * 3+2]--;
				}
			} else
				i++;
		}
		a_vertex = verticlesList.ToArray ();
		totalVerticle = a_vertex.Length;

		// transform verticle acording target mesh's transformation
		length = totalVerticle;
		if (meshScale != Vector3.one) {
			for (i = 0; i < length; i++) {
				a_vertex [i].x = a_vertex [i].x * meshScale.x;
				a_vertex [i].y = a_vertex [i].y * meshScale.y;
				a_vertex [i].z = a_vertex [i].z * meshScale.z;
			}
		}

		if (meshRotation != Quaternion.identity) {
			for (i = 0; i < length; i++) {
				a_vertex [i] = meshRotation*a_vertex [i];
			}
		}

		if (meshPosition != Vector3.zero){
			for (i = 0; i < length; i++) {
				a_vertex [i] = a_vertex [i] + meshPosition;
			}
		}

		// init list of triangles belong to a specific verticle

		trisBelongToVerticle = new List<int>[length];

		for (i = 0; i < length; i++) {
			trisBelongToVerticle [i] = new List<int> ();
		}

		// create triangle data list
		// assign triangles to verticles they belong to
		// build an edge list

		List<Edge> edgeList = new List<Edge> ();


		length = totalTris;

		a_trisPlane = new Triangle[length];

		for (i = 0; i < totalTris; i++) {
			a_trisPlane [i] = new Triangle (a_vertex [a_triangle [i*3]], a_vertex [a_triangle [i*3 + 1]], a_vertex [a_triangle [i*3 + 2]]);
			if (!trisBelongToVerticle [a_triangle [i * 3]].Contains(i))
				trisBelongToVerticle [a_triangle [i * 3]].Add (i);
			if (!trisBelongToVerticle [a_triangle [i * 3+1]].Contains (i))
				trisBelongToVerticle [a_triangle [i * 3+1]].Add (i);
			if (!trisBelongToVerticle [a_triangle [i * 3+2]].Contains (i))
				trisBelongToVerticle [a_triangle [i * 3+2]].Add (i);
			AppendEdge (ref edgeList, new Edge(a_triangle [i*3],a_triangle [i*3+1]));
			AppendEdge (ref edgeList, new Edge(a_triangle [i*3+1],a_triangle [i*3+2]));
			AppendEdge (ref edgeList, new Edge(a_triangle [i*3+2],a_triangle [i*3]));
		}

		// convert edgeList into a array for faster process
		a_edge = edgeList.ToArray();
		length = a_edge.Length;
		trisBelongToEdge = new int[length][];
		// now find all the tris (max 2) belong to an edge
		for (i = 0; i < length; i++) {
			counter = 0;
			trisBelongToEdge [i] = new int[2];
			trisBelongToEdge [i][0] = trisBelongToEdge [i][1] = -1;
			for (j = 0; j < trisBelongToVerticle [a_edge [i].p1].Count; j++) {
				if (a_trisPlane [trisBelongToVerticle [a_edge [i].p1] [j]].ContainVerticle (a_vertex [a_edge [i].p2])) {
					trisBelongToEdge [i][counter] = trisBelongToVerticle [a_edge [i].p1] [j];
					counter++;
					if (counter == 2)
						break;
				}
			}
		}

		// good, on the other hand, find edges that belong to a tris
		totalEdge = a_edge.Length;
		for (i = 0; i < totalTris; i++) {
			counter = 0;
			for (j = 0; j < totalEdge; j++) {
				if (a_edge [j].Contain (a_triangle [i * 3], a_triangle [i * 3 + 1])) {
					a_trisPlane [i].e[0] = j;
					counter++;
					if (counter == 3)
						break;
				}
				else if (a_edge [j].Contain (a_triangle [i * 3 + 1], a_triangle [i * 3 + 2])) {
					a_trisPlane [i].e[1] = j;
					counter++;
					if (counter == 3)
						break;
				}
				else if (a_edge [j].Contain (a_triangle [i * 3 + 2], a_triangle [i * 3])) {
					a_trisPlane [i].e[2] = j;
					counter++;
					if (counter == 3)
						break;
				}
			}
		}

		// prepare color map
		if (myColorArray == null || myColorArray.Length != mapSize * mapSize * mapSize)
			myColorArray = new Color32[mapSize * mapSize * mapSize];
		else {
			length = myColorArray.Length;
			for (i = 0; i<length; i++)
			{
				myColorArray [i].r = myColorArray [i].g = myColorArray [i].b = 0;
			}
		}

		// calculate voxel size and starting point position to start scan
		voxelSize = 1.0f / (float)mapSize;
		startPosition.x = voxelSize * (-1 * mapSize * 0.5f+0.5f);
		startPosition.y = voxelSize * 0.5f;
		startPosition.z = voxelSize * (mapSize * 0.5f - 0.5f);
		blendRadiusDivider = 1.0f / blendRadius;
	}

	public void AppendEdge(ref List<Edge> edgeList, Edge targetEdge)
	{
		int length = edgeList.Count;
		for (int i = 0; i < length; i++) {
			if (edgeList [i].Contain (targetEdge.p1, targetEdge.p2)) {
				return;
			}
		}
		edgeList.Add (targetEdge);
	}

	public void ReconstructMesh()
	{
		Mesh myMesh = new Mesh ();
		myMesh.vertices = a_vertex;
		myMesh.triangles = a_triangle;
		myMesh.RecalculateNormals ();
		reconstructMeshVisualize.mesh = myMesh;
	}

	public void BeginParalleScan()
	{
		threadAbort = false;

		if (a_thread == null || a_thread.Length != coreNumber) {
			a_thread = new Thread[coreNumber];
			dataScanDone = new bool[coreNumber];
			Debug.Log ("Scan use " + a_thread.Length + " threads.");
		}
		for (int i = 0; i < coreNumber; i++) {
			int currentI = i;
			dataScanDone [currentI] = false;
			a_thread[currentI] = StartTheThread (currentI);
		}
	}

	Thread StartTheThread(int startIndex)
	{
		Thread t = new Thread (() => LoopAssignThread (startIndex));
		t.Start ();
		return t;
	}

	void LoopAssignThread(int coreIndex)
	{
		if (!threadAbort) {
			int length = mapSize / coreNumber;
			for (int i = coreIndex * length; i < coreIndex * length + length; i++) {
				ScanLayerAt (i);
			}
			dataScanDone [coreIndex] = true;
		}
	}

	public void ScanLayerAt(int localIndex = -1)
	{
		bool createVisualBoard =false;
		if (localIndex == -1) {
			localIndex = layerToScan;
			createVisualBoard = true;
		}
		List<int> relateTrisIndex = new List<int> ();
		Vector3 localSampleVoxelPosition = Vector3.zero;
		localSampleVoxelPosition.z = voxelSize * (mapSize * 0.5f - 0.5f) - localIndex * voxelSize;
		int xI, yI, tI, localVerticleIndex, localEdgeIndex, myTotalTris;
		float localMinDistance = 1f;
		float localDistance, colorValue;
		Color32 tempColor = Color.black;
		Vector3 localTargetDistancePoint, localClosestPoint, localIntersectPoint;
		bool localOnVerticle, localOnEdge, localOutOfRange;

		// scan to eliminate all tris that completly irrelevante to this layer

		for (tI = 0; tI < totalTris; tI++) {
			if (!a_trisPlane [tI].Irrelevante (localSampleVoxelPosition.z, blendRadius)) {
				relateTrisIndex.Add (tI);
			}
		}
		myTotalTris = relateTrisIndex.Count;

		for (yI = 0; yI < mapSize; yI++) {
			localSampleVoxelPosition.y = voxelSize * 0.5f + yI * voxelSize;
			for (xI = 0; xI < mapSize; xI++) {
				localSampleVoxelPosition.x = voxelSize * (-1 * mapSize * 0.5f + 0.5f) + xI * voxelSize;
				localClosestPoint = localSampleVoxelPosition;
				localMinDistance = 1;

				for (tI = 0; tI < myTotalTris; tI++) {
					localTargetDistancePoint = a_trisPlane [relateTrisIndex[tI]].ClosestPoint (localSampleVoxelPosition);
					localDistance = Vector3.Distance (localSampleVoxelPosition, localTargetDistancePoint);
					if (Mathf.Abs (localDistance) < Mathf.Abs (localMinDistance)) {
						localClosestPoint = localTargetDistancePoint;
						localMinDistance = localDistance;
					}
				}

				if (localMinDistance > blendRadius)
					localOutOfRange = true;
				else
					localOutOfRange = false;
				
				Vector3 castVector = localClosestPoint - localSampleVoxelPosition;
				castVector = castVector.normalized;

				if (!localOutOfRange) {
					int triangleFace, triangleHit;
					triangleHit = triangleFace = 0;
					Vector3 localNormal = Vector3.zero;
					int counter1, counter2;
					for (counter1 = 0; counter1 < totalTris; counter1++) {
						localOnVerticle = localOnEdge = false;
						localVerticleIndex = localEdgeIndex = -1;
						for (counter2 = 0; counter2 < 3; counter2++) {
							if (Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], localClosestPoint) < 0.0001f) {
								localOnVerticle = true;
								localVerticleIndex = a_triangle [counter1 * 3 + counter2];
								break;
							}
							if (counter2 < 2) {
								if (Mathf.Abs (Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], localClosestPoint)
									+ Vector3.Distance (localClosestPoint, a_vertex [a_triangle [counter1 * 3 + counter2 + 1]])
									- Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], a_vertex [a_triangle [counter1 * 3 + counter2 + 1]])) < 0.0001f) {
									localOnEdge = true;
									localEdgeIndex = a_trisPlane [counter1].e [counter2];
									break;
								}
							} else {
								if (Mathf.Abs (Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], localClosestPoint)
									+ Vector3.Distance (localClosestPoint, a_vertex [a_triangle [counter1 * 3]])
									- Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], a_vertex [a_triangle [counter1 * 3]])) < 0.0001f) {
									localOnEdge = true;
									localEdgeIndex = a_trisPlane [counter1].e [counter2];
									break;
								}
							}
						}
						if (!localOnVerticle && !localOnEdge) {
							if (!a_trisPlane [counter1].IntersectWithRay (localSampleVoxelPosition, localClosestPoint + castVector * 0.01f)) {
								continue;
							}
						}
						triangleHit++;
						if (localOnEdge) {
							localNormal = (a_trisPlane [trisBelongToEdge [localEdgeIndex] [0]].NormalVector () + a_trisPlane [trisBelongToEdge [localEdgeIndex] [1]].NormalVector ()).normalized;
						} else if (localOnVerticle) {
							localNormal.x = localNormal.y = localNormal.z = 0;
							for (counter2 = 0; counter2 < trisBelongToVerticle [localVerticleIndex].Count; counter2++) {
								localNormal += a_trisPlane [trisBelongToVerticle [localVerticleIndex] [counter2]].NormalVector ();
							}
							localNormal = localNormal.normalized;
						} else
							localNormal = a_trisPlane [counter1].NormalVector ();
						if (Vector3.Dot (localNormal, castVector) <= angleCosinThreshold)
							triangleFace++;
					}

					if (triangleHit * 0.5f <= triangleFace) {
						localMinDistance = -1 * Mathf.Abs (localMinDistance);
					} else {
						localMinDistance = Mathf.Abs (localMinDistance);
					}
				}
				else
					localMinDistance = -1;

				colorValue = Mathf.Clamp01 ((localMinDistance * blendRadiusDivider + 1) * 0.5f);

				tempColor.r = tempColor.g = tempColor.b = (byte)(colorValue * 255);

				myColorArray [xI + yI * mapSize + localIndex * mapSize * mapSize] = tempColor;

				if (createVisualBoard) {
					visualVoxel.transform.position = localSampleVoxelPosition;
					GameObject gO = Instantiate (visualVoxel);
					gO.transform.position = visualVoxel.transform.position;
					gO.GetComponent<SpriteRenderer> ().color = tempColor;
				}
			}
		}

		// recalculate for map
		for (yI = 0; yI < mapSize; yI++) {
			for (xI = 0; xI < mapSize; xI++) {
				if (myColorArray [xI + yI * mapSize + localIndex * mapSize * mapSize].r == 0) {
					if (xI>0&&myColorArray [(xI-1) + yI * mapSize + localIndex * mapSize * mapSize].r>=127)
					{
						tempColor.r = tempColor.g = tempColor.b = 255;

						myColorArray [xI + yI * mapSize + localIndex * mapSize * mapSize] = tempColor;
					} else if (xI+1<mapSize&&myColorArray [(xI+1) + yI * mapSize + localIndex * mapSize * mapSize].r>=127)
					{
						tempColor.r = tempColor.g = tempColor.b = 255;
						myColorArray [xI + yI * mapSize + localIndex * mapSize * mapSize] = tempColor;
					}
				}
			}
		}
	}

	void ScanLayerAnimation()
	{
		Vector3 localSampleVoxelPosition = Vector3.zero;
		localSampleVoxelPosition.z = voxelSize * (mapSize * 0.5f - 0.5f) - layerToScan * voxelSize;
		float colorValue;
		Color32 tempColor = Color.black;

		localSampleVoxelPosition.y = voxelSize * 0.5f + gY * voxelSize;
		localSampleVoxelPosition.x = voxelSize * (-1 * mapSize * 0.5f+0.5f) + gX * voxelSize;
		visualVoxel.transform.position = localSampleVoxelPosition;
		for (int i = 0; i<totalTris; i++)
		{
			targetDistancePoint = a_trisPlane [i].ClosestPoint(localSampleVoxelPosition);
			gDistance = Vector3.Distance (localSampleVoxelPosition, targetDistancePoint);
			if (Mathf.Abs (gDistance) < Mathf.Abs (gMinDistance)) {
				closestPoint = targetDistancePoint;
				gMinDistance = gDistance;
			}
		}
		/*
		gT++;
		if (gT >= totalTris) {
			Vector3 castVector = closestPoint - localSampleVoxelPosition;
			int maxHit = Physics.RaycastNonAlloc (localSampleVoxelPosition, castVector, hit, castVector.magnitude);
			int triangleFace, triangleHit;
			triangleHit = triangleFace = 0;
			for (int i = 0; i < maxHit; i++) {
				triangleHit++;
				if (Vector3.Dot (hit [i].normal, castVector) >= 0)
					triangleFace++;
			}
			if (triangleHit * 0.5f >= triangleFace)
				gMinDistance = -1 * Mathf.Abs (gMinDistance);
			else
				gMinDistance = Mathf.Abs (gMinDistance);

			colorValue = Mathf.Clamp01 ((gMinDistance * blendRadiusDivider + 1) * 0.5f);

			tempColor.r = tempColor.g = tempColor.b = (byte)(colorValue * 255);

			gX++;
			gMinDistance = 1;
			gT = 0;

			GameObject gO = Instantiate (visualVoxel);
			gO.transform.position = visualVoxel.transform.position;
			gO.GetComponent<SpriteRenderer> ().color = tempColor;


		}
		*/


		Vector3 castVector = closestPoint - localSampleVoxelPosition;
		castVector = castVector.normalized;

		Debug.DrawLine (localSampleVoxelPosition, closestPoint);

		int triangleFace, triangleHit;
		triangleHit = triangleFace = 0;
		Vector3 localNormal = Vector3.zero;
		int counter1 ,counter2;
		//Debug.Log ("New voxel");
		for (counter1 = 0; counter1 < totalTris; counter1++) {
			onVerticle = onEdge = false;
			for (counter2 = 0; counter2 < 3; counter2++) {
				if (Vector3.Distance (a_vertex [a_triangle [counter1 * 3+counter2]], closestPoint) < 0.0001f) {
					onVerticle = true;
					verticleCutIndex = a_triangle [counter1 * 3 + counter2];
					break;
				}
				if (counter2 < 2) {
					if (Mathf.Abs (Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], closestPoint)
					    + Vector3.Distance (closestPoint, a_vertex [a_triangle [counter1 * 3 + counter2 + 1]])
					    - Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], a_vertex [a_triangle [counter1 * 3 + counter2 + 1]])) < 0.0001f) {
						onEdge = true;
						edgeCutIndex = a_trisPlane [counter1].e [counter2];
						break;
					}
				} else {
					if (Mathf.Abs (Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], closestPoint)
						+ Vector3.Distance (closestPoint, a_vertex [a_triangle [counter1 * 3]])
						- Vector3.Distance (a_vertex [a_triangle [counter1 * 3 + counter2]], a_vertex [a_triangle [counter1 * 3]])) < 0.0001f) {
						onEdge = true;
						edgeCutIndex = a_trisPlane [counter1].e [counter2];
						break;
					}
				}
			}
			if (!onVerticle&&!onEdge) {
				if (!a_trisPlane [counter1].IntersectWithRay (localSampleVoxelPosition, closestPoint + castVector * 0.001f)) {
					continue;
				}
			}
			triangleHit++;
			if (onEdge) {
				localNormal = (a_trisPlane [trisBelongToEdge [edgeCutIndex] [0]].NormalVector () + a_trisPlane [trisBelongToEdge [edgeCutIndex] [1]].NormalVector ()).normalized;
			} else if (onVerticle) {
				localNormal.x = localNormal.y = localNormal.z = 0;
				for (counter2 = 0; counter2< trisBelongToVerticle[verticleCutIndex].Count;counter2++)
				{
					localNormal += a_trisPlane [trisBelongToVerticle [verticleCutIndex] [counter2]].NormalVector ();
				}
				localNormal = localNormal.normalized;
			} else
				localNormal = a_trisPlane [counter1].NormalVector ();
			if (Vector3.Dot (localNormal, castVector) <= angleCosinThreshold)
				triangleFace++;
			/*else {
				print ("Tris number: "+counter1+" Angle: "+Mathf.Acos(Vector3.Dot (localNormal, castVector))*Mathf.Rad2Deg);

				if (onEdge)
					print ("Edge number: "+edgeCutIndex);
				if (onVerticle)
					print ("Verticle number: "+verticleCutIndex);
			}*/
		}

		if (triangleHit * 0.5f <= triangleFace) {
			gMinDistance = -1 * Mathf.Abs (gMinDistance);
		} else {
			gMinDistance = Mathf.Abs (gMinDistance);
		}

		Debug.Log (gMinDistance.ToString()+" Hit: "+triangleHit+" Face: "+triangleFace);
		if (triangleHit == 0)//||triangleFace==0) {
		{
			Debug.DrawLine (localSampleVoxelPosition, closestPoint + castVector * 0.001f, Color.red);
			Debug.LogError ("Wait");
		}

		colorValue = Mathf.Clamp01 ((gMinDistance * blendRadiusDivider + 1) * 0.5f);

		tempColor.r = tempColor.g = tempColor.b = (byte)(colorValue * 255);

		gX++;
		gMinDistance = 1;

		GameObject gO = Instantiate (visualVoxel);
		gO.transform.position = visualVoxel.transform.position;
		gO.GetComponent<SpriteRenderer> ().color = tempColor;


		if (gX >= mapSize) {
			gX = 0;
			gY++;
		}

	}
}