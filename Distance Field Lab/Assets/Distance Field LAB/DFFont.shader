﻿Shader "Unlit/DFFont"
{
	Properties
	{
		_MainTex ("Origin Texture", 2D) = "white" {}
		_SecondTex ("Target Texture", 2D) = "black" {}
		_Interpolate("Lerp", Range(0,1)) = 0
		_AlphaThreshold("Threshold", Range(0,1)) = 0.125
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _SecondTex;
			float4 _SecondTex_ST;
			float _AlphaThreshold;
			float _Interpolate;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed mainDFValue = tex2D(_MainTex, i.uv).r;
				fixed secondDFValue = tex2D(_SecondTex, i.uv).r;
				fixed theDistance = lerp(mainDFValue, secondDFValue, _Interpolate);
				fixed alpha = smoothstep(0.5-_AlphaThreshold, 0.5+_AlphaThreshold, theDistance);
				return fixed4(1,1,1,alpha);
			}
			ENDCG
		}
	}
}
