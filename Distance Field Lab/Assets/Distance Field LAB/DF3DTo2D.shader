﻿Shader "Unlit/DF3DTo2D"
{
	Properties
	{
		_MainTex ("Origin Texture", 3D) = "white" {}
		_AlphaThreshold("Threshold", Range(0,1)) = 0.125
		_Depth ("Depth", Range(0,1)) = 0.5
		_RedValue ("Red", int) = 1
		_GreenValue ("Green", int) = 1
		_BlueValue ("Blue", int) = 1
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler3D _MainTex;
			float _AlphaThreshold;
			float _Depth;
			float _RedValue;
			float _GreenValue;
			float _BlueValue;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 data = tex3D(_MainTex, float3(i.uv, _Depth));
				fixed theDistance = data.r;
				fixed alpha = smoothstep(0.5-_AlphaThreshold, 0.5+_AlphaThreshold, theDistance);
				return fixed4(data.r*_RedValue, data.g*_GreenValue, data.b*_BlueValue,1);
			}
			ENDCG
		}
	}
}
