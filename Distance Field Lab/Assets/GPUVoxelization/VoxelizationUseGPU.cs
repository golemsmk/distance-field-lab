﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelSystem;

public class VoxelizationUseGPU : MonoBehaviour {

	public ComputeShader voxelizer;

	public Mesh targetMesh;

	public RenderTexture myTexure;

	public void StartVoxel()
	{
		GPUVoxelData data = GPUVoxelizer.Voxelize (voxelizer, targetMesh, 64, true);

		RenderTexture localTexture = GPUVoxelizer.BuildTexture3D(voxelizer, data, RenderTextureFormat.ARGB32, FilterMode.Bilinear);

		data.Dispose();

		Graphics.CopyTexture (localTexture, myTexure);
	}

}
