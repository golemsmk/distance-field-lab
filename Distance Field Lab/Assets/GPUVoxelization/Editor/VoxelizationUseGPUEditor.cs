﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(VoxelizationUseGPU))]
public class VoxelizationUseGPUEditor : Editor {

	VoxelizationUseGPU myTarget;

	public override void OnInspectorGUI ()
	{
		myTarget = (VoxelizationUseGPU)target;
		DrawDefaultInspector ();
		if (GUILayout.Button ("StartVoxel")) {
			myTarget.StartVoxel ();
		}
	}
}
